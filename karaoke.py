#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from xml.sax import make_parser
from smil import SMILHandler
import json


def to_string(tags):
    painting = ""
    for dicc in tags:
        name = dicc["name"]
        attrs = dicc["attrs"]
        painting = painting + name
        for atributos in attrs:
            atributo = atributos[0]
            value = atributos[1]
            painting = painting + "\t" + atributo + '="' + value + '"'
        painting = painting + "\n"
    return painting


def to_json(tags):
    longitud = 0
    painting = "[\n"
    painting = painting + "\t" + "{"
    for dicc in tags:
        name = dicc["name"]
        attrs = dicc["attrs"]
        painting = painting + '"name": "' + name + '",\n'
        painting = painting + '\t "attrs": {\n'
        for atributos in attrs:
            atributo = atributos[0]
            value = atributos[1]
            painting = painting + '\t  "' + atributo + '": "' + value + '",\n'
        longitud = longitud + 1
        if longitud == (len(tags)):
            painting = painting + '\t  }\n' + '\t}\n'
        else:
            painting = painting + '\t  }\n' + '\t},\n' + '\t{'
    painting = painting + ']'
    return painting


def do_json(tags, filesmil, filejson=''):
    for tag in tags:
        name = tag["name"]
        attrs = tag["attrs"]
        diccionario = dict(attrs)
        del tag["attrs"]
        tag.clear()
        tag["attrs"] = diccionario
        tag["name"] = name
    if filejson == '':
        filejson = filesmil.replace('.smil', '.json')
    with open(filejson, 'w') as jsonfile:
        json.dump(tags, jsonfile, indent=3)


def main():
    """Programa principal"""
    if len(sys.argv) == 2:
        try:
            file = sys.argv[1]
            json = False
        except IndexError:
            sys.exit('Usage python3 karaoke.py file.smil')
    else:
        try:
            if sys.argv[1] == "--json":
                file = sys.argv[2]
                json = True
            else:
                sys.exit('Usage python3 karaoke.py --json file.smil')
        except IndexError:
            file = sys.argv[2]
            sys.exit('Usage python3 karaoke.py --json file.smil')

    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open(file))
    tags = cHandler.get_tags()
    if len(sys.argv) == 2:
        print(to_string(tags))

    if json:
        print(to_json(tags))
        do_json(tags, "karaoke.json")


if __name__ == "__main__":
    main()
