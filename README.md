En el ejercicio 4, me pasan dos tests, pero el tercero no he conseguido que me elimine los dos atributos end y dur
que estan vacios al pasar otros ficheros smil por el programa. Pero con el fichero karaoke.smil lo hace perfecto.

Hay dos formas de ejecutar karaoke.py:
    1. Si ejecutas "karaoke.py karaoke.smil". Te saca por pantalla lo del ejercicio5
    2. Si ejecutas "karaoke.oy --json karaoke.smil". Te saca por pantalla un string en formato json (ejercicio6) Lo he hecho según 
lo dicta el enunciado. Y además te crea un archivo .json

En el ejercicio7 me descarga todos los ficheros y los guarda bien (fichero-0, fichero-1, etc) pero aun asi no me pasa el test.

Repositorio de plantilla para la práctica 3 (XML y JSON) de PTAVI

Recuerda que antes de empezar a trabajar con esta practica deberías realizar un fork de este repositorio, con lo que crearás un nuevo repositorio en tu cuenta, cuyos contenidos serán una copia de los de este. Luego tendrás que clonar ese repositorio para tener una copia local con la que podrás trabajar. No olvides luego hacer commits frecuentes, y sincronizarlos con el servidor (`git push`) antes de la fecha de entrega final de la práctica. Recuerda también que para que pueda ser corregido, el repositorio deberá ser público a partir de la fecha de entrega.

Para hacer esta práctica, consulta su [enunciado](https://gitlab.com/cursomminet/code/-/blob/master/p3-xml/ejercicios.md).


