#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from smil import SMILHandler
import smil
import os
import urllib
import json


class Karaoke(SMILHandler):

    def __init__(self, file):
        self.tags = []
        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.tags = cHandler.get_tags()

    def download(self):
        n = 0
        for datos in self.tags:
            attrs = datos["attrs"]
            for atributos in attrs:
                atributo = atributos[0]
                value = atributos[1]
                if atributo == 'src':
                    if value.startswith('http://'):
                        url = value
                        archivo = url.split('/')[-1]
                        ext = url.split('.')[-1]
                        urllib.request.urlretrieve(url, archivo)
                        archivo_downloaded = "/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-p3-21/" + archivo
                        archivo_cambiado = "/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-p3-21/" \
                                           + "fichero-" + str(n) + "." + ext
                        os.rename(archivo_downloaded, archivo_cambiado)
                        n = n + 1

    def __str__(self):
        painting = ""
        for dicc in self.tags:
            name = dicc["name"]
            attrs = dicc["attrs"]
            painting = painting + name
            for atributos in attrs:
                atributo = atributos[0]
                value = atributos[1]
                painting = painting + "\t" + atributo + '="' + value + '"'
            painting = painting + "\n"
        return painting

    def to_json(self):
        longitud = 0
        painting = "[\n"
        painting = painting + "\t" + "{"
        for dicc in self.tags:
            name = dicc["name"]
            attrs = dicc["attrs"]
            painting = painting + '"name": "' + name + '",\n'
            painting = painting + '\t "attrs": {\n'
            for atributos in attrs:
                atributo = atributos[0]
                value = atributos[1]
                painting = painting + '\t  "' + atributo + '": "' + value + '",\n'
            longitud = longitud + 1
            if longitud == (len(self.tags)):
                painting = painting + '\t  }\n' + '\t}\n'
            else:
                painting = painting + '\t  }\n' + '\t},\n' + '\t{'
        painting = painting + ']'
        return painting

    def do_json(self, filesmil, filejson=''):
        for tag in self.tags:
            name = tag["name"]
            attrs = tag["attrs"]
            diccionario = dict(attrs)
            del tag["attrs"]
            tag.clear()
            tag["attrs"] = diccionario
            tag["name"] = name
        if filejson == '':
            filejson = filesmil.replace('.smil', '.json')
        with open(filejson, 'w') as jsonfile:
            json.dump(self.tags, jsonfile, indent=3)


def main():
    """Programa principal"""
    if len(sys.argv) == 2:
        try:
            file = sys.argv[1]
            json = False
        except IndexError:
            sys.exit('Usage: python3 karaoke.py file.smil')
    else:
        try:
            if sys.argv[1] == "--json":
                file = sys.argv[2]
                json = True
            else:
                sys.exit('Usage: python3 karaoke.py --json file.smil')
        except IndexError:
            file = sys.argv[2]
            sys.exit('Usage: python3 karaoke.py --json file.smil')

    descargar = Karaoke(file)

    if len(sys.argv) == 2:
        print(descargar.__str__())

    if json:
        print(descargar.to_json())
        descargar.do_json(file, "karaoke.json")

    descargar.download()


if __name__ == "__main__":
    main()
