#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from smil import SMILHandler
import smil
import os
import urllib


class Descargar(SMILHandler):

    def __init__(self, file):
        parser = make_parser()
        cHandler = SMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(file))
        self.lista = cHandler.get_tags()

    def do_local(self):
        n = 0
        for datos in self.lista:
            attrs = datos["attrs"]
            for atributos in attrs:
                atributo = atributos[0]
                value = atributos[1]
                if atributo == 'src':
                    if value.startswith('http://'):
                        url = value
                        archivo = url.split('/')[-1]
                        ext = url.split('.')[-1]
                        urllib.request.urlretrieve(url, archivo)
                        archivo_downloaded = "/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-p3-21/" + archivo
                        archivo_cambiado = "/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-p3-21/" \
                                           + "fichero-" + str(n) + "." + ext
                        os.rename(archivo_downloaded, archivo_cambiado)
                        n = n + 1


def main():
    """Programa principal"""
    try:
        file = sys.argv[1]
    except IndexError:
        sys.exit('Usage python3 karaoke.py file.smil')

    descargar = Descargar(file)
    descargar.do_local()


if __name__ == "__main__":
    main()
