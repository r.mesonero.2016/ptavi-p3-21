#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.contador = 0
        self.haynombre = ""

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':
            self.calificacion = attrs.get('calificacion', "")
            self.contador = self.contador + 1
            print("Chiste número: " + str(self.contador) + " (" + attrs["calificacion"] +
                  ")")
            self.haynombre = name
        elif name == 'pregunta':
            self.pregunta = ""
            self.inPregunta = True
            self.haynombre = name
        elif name == 'respuesta':
            self.respuesta = ""
            self.inRespuesta = True
            self.haynombre = name

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'pregunta':
            self.inPregunta = False
        elif name == 'respuesta':
            self.inRespuesta = False
        elif name == 'chiste':
            self.pregunta = ""
            self.respuesta = ""

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """
        if self.inPregunta:
            self.pregunta = self.pregunta + char
        elif self.inRespuesta:
            self.respuesta += char

        if self.pregunta != "":
            if self.respuesta != "":
                if char == "\n":
                    if self.haynombre == 'respuesta':
                        print("Pregunta: " + self.pregunta)
                        print("Respuesta: " + self.respuesta)
                        print()
                        self.haynombre = "seacaboelchiste"
                    if self.haynombre == "pregunta":
                        print("Pregunta: " + self.pregunta)
                        print("Respuesta: " + self.respuesta)
                        print()
                        self.haynombre = "seacaboelchiste"


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes.xml'))


if __name__ == "__main__":
    main()
