#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):
    def __init__(self):
        self.result = ""
        # diccionarios
        self.root_layout = {}
        self.region = {}
        self.imagen = {}
        self.audio = {}
        self.textstream = {}
        self.diccionario = {}
        # lista
        self.lista = []

    def startElement(self, nombre, atributo):
        if nombre == "root-layout":
            self.root_layout["width"] = atributo.get("width", "")
            self.root_layout["height"] = atributo.get("height", "")
            self.root_layout["background-color"] = \
                atributo.get("background-color", "")
            mylist = self.root_layout.items()
            mylist = list(mylist)
            contador = 0
            for borrar in mylist:
                if borrar[1] == '':
                    mylist.pop(contador)
                contador += 1
            self.diccionario["attrs"] = mylist
            self.diccionario["name"] = nombre
            self.lista.append(self.diccionario)
            self.root_layout = {}
            self.diccionario = {}
        elif nombre == "region":
            self.region["id"] = atributo.get("id", "")
            self.region["top"] = atributo.get("top", "")
            self.region["bottom"] = atributo.get("bottom", "")
            self.region["left"] = atributo.get("left", "")
            self.region["right"] = atributo.get("right", "")
            mylist = self.region.items()
            mylist = list(mylist)
            contador = 0
            for borrar in mylist:
                if borrar[1] == '':
                    mylist.pop(contador)
                contador += 1
            self.diccionario["attrs"] = mylist
            self.diccionario["name"] = nombre
            self.lista.append(self.diccionario)
            self.diccionario = {}
            self.region = {}
        elif nombre == "img":
            self.imagen["src"] = atributo.get("src", "")
            self.imagen["region"] = atributo.get('region', "")
            self.imagen["begin"] = atributo.get("begin", "")
            self.imagen["end"] = atributo.get("end", "")
            self.imagen["dur"] = atributo.get("dur", "")
            mylist = self.imagen.items()
            mylist = list(mylist)
            contador = 0
            for borrar in mylist:
                if borrar[1] == '':
                    mylist.pop(contador)
                contador += 1
            self.diccionario["attrs"] = mylist
            self.diccionario["name"] = nombre
            self.lista.append(self.diccionario)
            self.diccionario = {}
            self.imagen = {}
        elif nombre == "audio":
            self.audio["src"] = atributo.get("src", "")
            self.audio["begin"] = atributo.get("begin", "")
            self.audio["dur"] = atributo.get("dur", "")
            mylist = self.audio.items()
            mylist = list(mylist)
            contador = 0
            for borrar in mylist:
                if borrar[1] == '':
                    mylist.pop(contador)
                contador += 1
            self.diccionario["attrs"] = mylist
            self.diccionario["name"] = nombre
            self.lista.append(self.diccionario)
            self.diccionario = {}
            self.audio = {}
        elif nombre == "textstream":
            self.textstream["src"] = atributo.get("src", "")
            self.textstream["region"] = atributo.get("region", "")
            self.textstream["fill"] = atributo.get("fill", "")
            mylist = self.textstream.items()
            mylist = list(mylist)
            contador = 0
            for borrar in mylist:
                if borrar[1] == '':
                    mylist.pop(contador)
                contador += 1
            self.diccionario["attrs"] = mylist
            self.diccionario["name"] = nombre
            self.lista.append(self.diccionario)
            self.diccionario = {}
            self.textstream = {}

    def get_tags(self):
        return self.lista


def main():
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())


if __name__ == "__main__":
    main()
